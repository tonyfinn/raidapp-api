"""add_extra_tables

Revision ID: 549c37f593f
Revises: 33fb4d9c571
Create Date: 2015-10-20 01:32:52.436284

"""

# revision identifiers, used by Alembic.
revision = '549c37f593f'
down_revision = '33fb4d9c571'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('raid_team',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('event',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('raid_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['raid_id'], ['raid.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('character',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('team_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['team_id'], ['raid_team.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('invite',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('event_id', sa.Integer(), nullable=True),
    sa.Column('character_id', sa.Integer(), nullable=True),
    sa.Column('_status', sa.Enum('accepted', 'invited', 'maybe', 'rejected', name='InviteStatus'), nullable=True),
    sa.ForeignKeyConstraint(['event_id'], ['event.id'], ),
    sa.ForeignKeyConstraint(['character_id'], ['character.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('character_job',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('level', sa.Integer(), nullable=True),
    sa.Column('character_id', sa.Integer(), nullable=True),
    sa.Column('job_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['job_id'], ['job.id'], ),
    sa.ForeignKeyConstraint(['character_id'], ['character.id'], ),
    sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('player_job')
    op.drop_table('invite')
    op.drop_table('character')
    op.drop_table('event')
    op.drop_table('raid_team')
