"""empty message

Revision ID: 5c54b58dd0
Revises: None
Create Date: 2015-10-16 23:50:32.232878

"""

# revision identifiers, used by Alembic.
revision = '5c54b58dd0'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('job',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('short_name', sa.String(), nullable=True),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('_role', sa.Enum('healer', 'tank', 'dps', 'any', name='Role'), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('short_name')
    )
    op.create_table('raid',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('short_name', sa.String(), nullable=True),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('size', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('raid')
    op.drop_table('job')
