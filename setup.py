#!/usr/bin/env python3
import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

from raidapp import __author__, __version__

class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', 'Arguments to pass to py.test')]

    def initialize_options(self):
        super().initialize_options()
        self.pytest_args = []

    def finalize_options(self):
        super().finalize_options()
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

def main():
    with open('README.rst') as readme_file:
        long_description = readme_file.read()

    setup(
        name='RaidApp',
        author=__author__,
        version=__version__,
        description='Raid planning webapp API',
        long_description=long_description,
        license='GPL',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Programming Language :: Python :: 3.4',
            'Programming Language :: Python :: 3.5',
        ],
        cmdclass = {'test': PyTest},
        packages=find_packages(exclude=['docs', 'tests']),
        data_files=[
            ('migrations', ['*', 'versions/*'])
        ],
        install_requires=[
            'alembic',
            'docopt',
            'Flask',
            'Flask-SQLAlchemy',
            'Flask-Migrate',
            'Flask-Script',
            'sqlalchemy'
        ],
        tests_require=['pytest', 'pytest-cov']
    )

if __name__ == '__main__':
    main()