"""add initial servers

Revision ID: 5acafd2845
Revises: 2e4310ca4c1
Create Date: 2015-10-31 21:31:55.854071

"""

# revision identifiers, used by Alembic.
revision = '5acafd2845'
down_revision = '2e4310ca4c1'

from alembic import op
import sqlalchemy as sa

from raidapp.app import db
from raidapp.models import Server


def upgrade():
    initial_data = [
        dict(name='Cerberus', region='EU'),
        dict(name='Lich', region='EU'),
        dict(name='Moogle', region='EU'),
        dict(name='Odin', region='EU'),
        dict(name='Phoenix', region='EU'),
        dict(name='Ragnarok', region='EU'),
        dict(name='Shiva', region='EU'),
        dict(name='Zodiark', region='EU'),
        dict(name='Balmung', region='NA'),
        dict(name='Gilgamesh', region='NA'),
        dict(name='Masamune', region='JP'),
        dict(name='Tonberry', region='JP')
    ]

    op.bulk_insert(Server.__table__, initial_data)

def downgrade():
    pass
