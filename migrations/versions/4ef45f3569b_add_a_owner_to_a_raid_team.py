"""Add a owner to a raid team

Revision ID: 4ef45f3569b
Revises: 5817db11e41
Create Date: 2015-10-30 20:53:45.893706

"""

# revision identifiers, used by Alembic.
revision = '4ef45f3569b'
down_revision = '5817db11e41'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('raid_team', schema=None) as batch_op:
        batch_op.add_column(sa.Column('owner_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key('raid_team_owner_id', 'user', ['owner_id'], ['id'])

    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('raid_team', schema=None) as batch_op:
        batch_op.drop_constraint('raid_team_owner_id', type_='foreignkey')
        batch_op.drop_column('owner_id')

    ### end Alembic commands ###
