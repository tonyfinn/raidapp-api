"""add_intial_ff14_data

Revision ID: 33fb4d9c571
Revises: 5c54b58dd0
Create Date: 2015-10-17 00:12:08.466261

"""

# revision identifiers, used by Alembic.
revision = '33fb4d9c571'
down_revision = '5c54b58dd0'

from alembic import op
import sqlalchemy as sa

from raidapp.app import db
from raidapp.models import Raid, Job, Role

def upgrade():
    initial_raids = [
        dict(id=1, short_name='A1S', name='Oppressor (Savage)', size=8),
        dict(id=2, short_name='A2S', name='The Cuff of the Father (Savage)', size=8),
        dict(id=3, short_name='A3S', name='Living Liquid (Savage)', size=8),
        dict(id=4, short_name='A4S', name='Manipulator (Savage)', size=8)
    ]
    initial_jobs = [
        dict(id=1, short_name='PLD', name='Paladin', _role='tank'),
        dict(id=2, short_name='WAR', name='Warrior', _role='tank'),
        dict(id=3, short_name='DRK', name='Dark Knight', _role='tank'),
        dict(id=4, short_name='SCH', name='Scholar', _role='healer'),
        dict(id=5, short_name='WHM', name='White Mage', _role='healer'),
        dict(id=6, short_name='AST', name='Astrologian', _role='healer'),
        dict(id=7, short_name='NIN', name='Ninja', _role='dps'),
        dict(id=8, short_name='DRG', name='Dragoon', _role='dps'),
        dict(id=9, short_name='SMN', name='Summoner', _role='dps'),
        dict(id=10, short_name='BLM', name='Black Mage', _role='dps'),
        dict(id=11, short_name='BRD', name='Bard', _role='dps'),
        dict(id=12, short_name='MCH', name='Machinist', _role='dps')
    ]

    op.bulk_insert(Raid.__table__, initial_raids)
    op.bulk_insert(Job.__table__, initial_jobs)

def downgrade():
    pass
