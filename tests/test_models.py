import pytest

from raidapp.models import MemoryRaidAppModel, MissingFieldsError, TooManyFieldsError, ExtraFieldsError


class SomeRaidModel(MemoryRaidAppModel):
    fields = ['x', 'y', 'z']


class OtherRaidModel(MemoryRaidAppModel):
    fields = ['a', 'b']


def test_create_raid_model():
    my_model = SomeRaidModel(1, y=2, z=3)
    assert my_model.x == 1
    assert my_model.y == 2
    assert my_model.z == 3

def test_create_model_no_args():
    with pytest.raises(MissingFieldsError) as e:
        my_model = SomeRaidModel(1)

    assert e.value.missing_fields == {'y', 'z'}
    assert 'Missing fields: y, z' in str(e)

def test_create_model_extra_args():
    with pytest.raises(ExtraFieldsError) as e:
        my_model = SomeRaidModel(1, 2, 3, r='a', o='b')

    assert e.value.extra_fields == {'o', 'r'}
    assert 'Unexpected fields: o, r' in str(e)

def test_create_model_too_many_args():
    with pytest.raises(TooManyFieldsError) as e:
        my_model = SomeRaidModel(1, 2, 3, 4)

    assert 'recieved 4, expected 3' in str(e)


def test_to_json():
    my_model = SomeRaidModel(1, 2, OtherRaidModel(3, 4, id=0), id=1)
    json_data = my_model.to_json()
    assert json_data == {
        'type': 'someraidmodel',
        'attributes': {
            'x': 1,
            'y': 2,
            'z': {
                'type': 'otherraidmodel',
                'id': 0
            }
        },
        'id': 1
    }