__author__ = 'tony'
import re

first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')


class RecordWithInclusions:
    def __init__(self, record, inclusions):
        self.record = record
        self.inclusions = inclusions

    def to_json_result(self):
        return {
            'data': self.record,
            'included': self.inclusions
        }


def underscore_case(name):
    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


def type_key(cls, plural=True):
    from .models import RaidAppModel
    if isinstance(cls, type):
        base_name = cls.__name__
    elif isinstance(cls, RaidAppModel):
        base_name = type(cls).__name__
    elif isinstance(cls, str):
        base_name = cls
    else:
        base_name = str(cls)
    return underscore_case(base_name) + ('s' if plural else '')
