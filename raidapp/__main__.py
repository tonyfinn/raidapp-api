#!/usr/bin/env python3
"""Raid Planner

Usage:
  raidapp start [--init]
  raidapp debug [--init]
  raidapp db [--] [<migrate_args>...]
  raidapp (-h | --help)
  raidapp --version

Options:
  -h --help    Show this screen
  --version    Show version
  --init       Setup initial data

"""


__author__ = 'tony'
__version__ = '0.1.0'

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from docopt import docopt
from raidapp.app import app, db
from raidapp import models, routes

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':

    arguments = docopt(__doc__, version='Raid Planner {}'.format(__version__))
    if arguments['db']:
        manager.handle('raidapp', ['db'] + arguments['<migrate_args>'])
    elif arguments['start'] or arguments['debug']:
        app.run(debug=arguments['debug'])
