"""Add Raid invites and character servers

Revision ID: 32f0ca41977
Revises: 4ef45f3569b
Create Date: 2015-10-31 14:51:35.253383

"""

# revision identifiers, used by Alembic.
revision = '32f0ca41977'
down_revision = '4ef45f3569b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('server',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=True),
    sa.Column('region', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('team_invite',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('inviter_id', sa.Integer(), nullable=True),
    sa.Column('invitee_id', sa.Integer(), nullable=True),
    sa.Column('team_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['invitee_id'], ['character.id'], ),
    sa.ForeignKeyConstraint(['inviter_id'], ['user.id'], ),
    sa.ForeignKeyConstraint(['team_id'], ['raid_team.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    with op.batch_alter_table('character', schema=None) as batch_op:
        batch_op.add_column(sa.Column('server_id', sa.Integer(), nullable=True))
        batch_op.create_foreign_key('character_server_id', 'server', ['server_id'], ['id'])

    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('character', schema=None) as batch_op:
        batch_op.drop_constraint('character_server_id', type_='foreignkey')
        batch_op.drop_column('server_id')

    op.drop_table('team_invite')
    op.drop_table('server')
    ### end Alembic commands ###
