import uuid


class APIError(Exception):
    status_code = 500
    error_code = 1

    def __init__(self, message, log_message=None, field=None, pointer=None):
        self.message = message
        self.field = field
        self.pointer = pointer
        if not log_message:
            log_message = message
        self.id = uuid.uuid4()
        super().__init__()

    def __str__(self):
        return '{} [{}]'.format(self.log_message, self.id)

    def to_dict(self):
        data = {
            'id': self.id,
            'code': self.error_code,
            'title': self.__class__.__name__,
            'detail': self.message,
            'status': self.status_code
        }

        if self.field and not self.pointer:
            self.pointer = '/data/attributes/' + self.field

        if self.pointer:
            data['source'] = {
                'pointer': self.pointer
            }

        return data



class AuthError(APIError):
    status_code = 403
    error_code = 3


class NotFoundError(APIError):
    status_code = 404
    error_code = 4


class InvalidLoginError(AuthError):
    error_code = 2


class InvalidTypeError(APIError):
    error_code = 5


class InvalidUpdateError(APIError):
    error_code = 6


class UserExistsError(AuthError):
    error_code = 7
