__author__ = 'tony'

from . import errors
from .app import api_route, db, app
from .models import Job, Raid, Event, User, Session, Character, RaidTeam, Server

from enum import Enum
from flask import request, session, g
from functools import wraps

from .utils import RecordWithInclusions


def require_login(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not session.get('uid', None):
            raise errors.AuthError('Login required')
        return f(*args, **kwargs)
    return wrapper


@api_route('/raids', methods=['GET'])
def list_raids():
    return Raid.all()


@api_route('/raids/<int:raid_id>', methods=['GET'])
def view_raid(raid_id):
    return Raid.by_id(raid_id)


@api_route('/jobs', methods=['GET'])
def list_jobs():
    return Job.all()


@api_route('/jobs/<int:job_id>', methods=['GET'])
def view_job(job_id):
    return Job.by_id(job_id)


@api_route('/raids/<int:raid_id>/events', methods=['GET'])
def list_events_for_raid(raid_id):
    return Event.where({'raid_id': raid_id}).all()


@api_route('/events/<int:event_id>', methods=['GET'])
def view_event(event_id):
    return Event.by_id(event_id)


@api_route('/events', methods=['GET'])
def list_events():
    return Event.all()


@api_route('/raid-teams', methods=['GET'])
def list_raid_teams():
    return RaidTeam.all()


@api_route('/raid-teams', methods=['POST'])
@require_login
def create_raid_team():
    team = request.get_json(force=True)
    team.owner = User.by_id(session['uid'])
    g.logger.info('Creating %s for user %s', repr(team), repr(team.owner))
    RaidTeam.save(team)
    db.session.commit()
    return team


@api_route('/raid-teams/<int:team_id>', methods=['GET'])
def get_raid_team(team_id):
    team = RaidTeam.by_id(team_id)
    g.logger.info('Loading %s', repr(team))
    return RecordWithInclusions(team, [team.owner])


@api_route('/raid-teams/<int:team_id>/owner', methods=['GET'])
def get_raid_team_owner(team_id):
    team = RaidTeam.by_id(team_id)
    g.logger.info('Loading owner for %s', repr(team))
    return team.owner


@api_route('/raid-teams/<int:team_id>/members', methods=['GET'])
def get_raid_team_members(team_id):
    team = RaidTeam.by_id(team_id)
    g.logger.info('Loading members for %s', repr(team))
    return team.members


@api_route('/events', methods=['POST'])
def create_event():
    model = request.get_json(force=True)
    Event.save(model)
    db.session.commit()


@api_route('/users', methods=['POST'])
def register():
    user = request.get_json(force=True)
    user.register()
    db.session.commit()


@api_route('/sessions', methods=['GET'])
def get_session():
    user_id = session.get('uid', None)
    if not user_id:
        raise errors.NotFoundError('User not logged in')
    else:
        g.logger.info('Found session for user {}'.format(user_id))
        return Session.by_id(user_id)


@api_route('/characters', methods=['GET'])
def list_characters():
    prefix = request.args.get('filter[startswith]', None)
    if prefix:
        return Character.startswith(prefix)
    else:
        return Character.all()


@api_route('/characters/<int:character_id>', methods=['GET'])
def show_character(character_id):
    return Character.by_id(character_id)


@api_route('/characters/<int:character_id>/jobs', methods=['GET'])
def list_character_jobs(character_id):
    character = Character.by_id(character_id)
    return list(character.jobs)


@api_route('/characters/<int:character_id>', methods=['PATCH'])
@require_login
def update_character(character_id):
    existing_character = Character.by_id(character_id)
    new_data = request.get_json(force=True)
    if existing_character.user_id == session['uid']:
        existing_character.patch(new_data)
        db.session.commit()
        return existing_character
    else:
        raise errors.AuthError('That character does not belong to you')


@api_route('/characters/<int:character_id>/team', methods=['GET'])
@require_login
def get_character_team(character_id):
    character = Character.by_id(character_id)
    return character.team

@api_route('/characters', methods=['POST'])
@require_login
def create_character():
    character = request.get_json(force=True)
    user = User.by_id(session['uid'])
    character.user = user
    g.logger.info('Creating character %s for user %s', repr(character), repr(user))
    Character.save(character)
    db.session.commit()
    return character


@api_route('/characters/<int:character_id>', methods=['DELETE'])
@require_login
def delete_character(character_id):
    character = Character.by_id(character_id)
    if character and character.user_id == session['uid']:
        db.session.delete(character)
        db.session.commit()
    else:
        if character:
            g.logger.warn('User %s tried to delete %s without permission', session['uid'], repr(character))
        raise errors.NotFoundError('No such character to delete')


@api_route('/sessions/<int:session_id>', methods=['DELETE'])
def logout(session_id):
    session.pop('uid')


@api_route('/sessions', methods=['POST'], sensitive=True)
def login():
    data = request.get_json(force=True)
    user = Session.login(data.email, data.password)
    session['uid'] = user.id
    return user


@api_route('/servers', methods=['GET'])
def list_servers():
    prefix = request.args.get('filter[startswith]', None)
    if prefix:
        return Server.startswith(prefix)
    else:
        return Server.all()


@api_route('/servers/<int:server_id>', methods=['GET'])
def view_server(server_id):
    return Server.by_id(server_id)


@api_route('/users/<int:user_id>', methods=['GET'])
@require_login
def view_user(user_id):
    if user_id != session['uid']:
        raise errors.AuthError('You many not view other\'s user date')
    return User.by_id(session['uid'])


@api_route('/health')
def hello_word():
    return {'status': 'ok'}

