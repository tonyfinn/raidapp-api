from enum import Enum
from sqlalchemy import Enum as SQLEnum, inspect
from sqlalchemy.orm import RelationshipProperty, ColumnProperty
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.associationproxy import association_proxy, AssociationProxy

from . import errors
from .config import config
from .utils import type_key, RecordWithInclusions
from .app import db, api_type

import bcrypt


class RaidAppModel:
    id = db.Column(db.Integer, primary_key=True)
    serialise_fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pending_relationships = {}

    def get_ref(self, item):
        return {
            'id': item.id,
            'type': type_key(item.__class__)
        }

    def get_link(self):
        return '{}/{}/{}'.format(config['api_root'], type_key(self).replace('_', '-'), self.id)

    def get_type_from_rel(self, key, rel):
        try:
            argument = rel.argument
        except AttributeError:
            prop = getattr(self.__class__, key)
            argument = prop.remote_attr.prop.argument
        try:
            return argument.arg
        except AttributeError:
            return argument.class_.__name__

    def get_relationship_data(self, key, rel, include=None):
        target_type = self.get_type_from_rel(key, rel)

        rel_data = {
            'links': {
                'self': '{}/relationships/{}'.format(self.get_link(), key.replace('_', '-')),
                'related': '{}/{}'.format(self.get_link(), key.replace('_', '-'))
            }
        }
        if not getattr(rel, 'uselist', True):
            value = getattr(self, key + '_id', None)
            if value:
                rel_data['data'] = {'id': value, 'type': type_key(target_type)}
            else:
                rel_data['data'] = None

        return rel_data

    def get_relationships_json(self, include=None):
        rels = {}
        for key, rel in self.relationships.items():
            if self.serialise_fields and key not in self.serialise_fields:
                continue
            rel_data = self.get_relationship_data(key, rel, include)
            if rel_data:
                rels[key] = rel_data
        return rels

    def to_json(self, include=None):
        json_obj = {
            'type': type_key(self.__class__, plural=True),
            'id': self.id,
            'attributes': {k: v for k, v in self.to_dict().items()},
            'links': {
                'self': self.get_link()
            }
        }

        rels = self.get_relationships_json(include)
        if rels:
            json_obj['relationships'] = rels

        for name, value in json_obj['attributes'].items():
            if isinstance(value, RaidAppModel):
                json_obj['attributes'][name] = self.get_ref(value)
        del json_obj['attributes']['id']

        return json_obj

    @classmethod
    def from_json(cls, data):
        if 'attributes' not in data:
            return cls.by_id(data['id'])

        attrs = data['attributes']
        kwargs = {field: attrs[field] for field in cls.fields if field in attrs}

        if 'id' in data:
            kwargs['id'] = int(data['id'])

        instance = cls(**kwargs)
        instance.pending_relationships = data.get('relationships', {})

        return instance

    def commit_relationships(self):
        for name, data in self.pending_relationships.items():
            if name in self.relationships:
                setattr(self, name, data)

    def patch(self, other):
        if not isinstance(other, type(self)):
            raise errors.InvalidTypeError('Can only patch similar objects. Tried {} with {}'.format(
                type_key(other, plural=False),
                type_key(self, plural=False)
            ))
        if other.id != self.id:
            raise errors.InvalidUpdateError('Tried to patch an object with a different instance')

        for field in self.fields:
            other_val = getattr(other, field, None)
            if other_val is not None:
                setattr(self, field, other_val)

        for rel in self.relationships.keys():
            other_val = other.pending_relationships.get(rel, getattr(other, rel, None))
            if other_val is not None:
                setattr(self, rel, other_val)

    def to_dict(self):
        if self.serialise_fields:
            fields = [f for f in self.serialise_fields]
        else:
            fields = {'id'}
            fields.update(self.fields)
        return {field: getattr(self, field) for field in fields}

    def __repr__(self):
        parts = []
        for k, v in self.to_dict().items():
            parts.append('{}={!r}'.format(k, v))
        return '{}({})'.format(
            self.__class__.__name__,
            ', '.join(parts))

    @classmethod
    def all(cls):
        return cls.query.all()

    @classmethod
    def by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def where(cls, **kwargs):
        return cls.query.filter_by(**kwargs)

    @classmethod
    def save(cls, item):
        if not isinstance(item, cls):
            raise errors.InvalidTypeError('Tried to save item of wrong type')
        item.commit_relationships()
        db.session.add(item)

    @classmethod
    def is_relationship(cls, attr):
        if isinstance(attr, RelationshipProperty):
            return True
        if isinstance(attr, ColumnProperty) and any(col.foreign_keys for col in attr.columns):
            return True
        return False

    @hybrid_property
    def fields(self):
        if isinstance(self, type):
            cls = self
        else:
            cls = type(self)

        data = inspect(cls)
        return {attr.key for attr in data.attrs if not cls.is_relationship(attr)}

    @hybrid_property
    def relationships(self):
        if isinstance(self, type):
            cls = self
        else:
            cls = type(self)

        info = inspect(cls)
        rels = {}
        rels.update(info.relationships)
        rels.update({k: v for k, v in info.all_orm_descriptors.items() if isinstance(v, AssociationProxy)})

        return rels


class RaidAppEnum(Enum):
    def to_json(self):
        return self.name

    @classmethod
    def to_sqla_enum(cls):
        return SQLEnum(*[value.name for value in cls], name=cls.__name__)

    @classmethod
    def from_db(cls, key):
        return getattr(cls, key)

    def format_for_db(self):
        return self.name


class Role(RaidAppEnum):
    healer = 1,
    tank = 2,
    dps = 4,
    any = 7


@api_type
class Job(RaidAppModel, db.Model):
    serialise_fields = ['id', 'short_name', 'name', 'roles']

    id = db.Column(db.Integer, primary_key=True)
    short_name = db.Column(db.String, unique=True)
    name = db.Column(db.String)
    _role = db.Column(Role.to_sqla_enum())

    def __str__(self):
        return self.name

    @property
    def roles(self):
        return [Role.from_db(self._role)]

    @roles.setter
    def roles(self, roles):
        role = roles[0]
        if isinstance(role, RaidAppEnum):
            self._role = role.format_for_db()
        else:
            self._role = role


@api_type
class CharacterJob(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    level = db.Column(db.Integer)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    character = db.relationship('Character', backref=db.backref('character_jobs'), order_by='Job.name', info={'include': True})
    job_id = db.Column(db.Integer, db.ForeignKey('job.id'))
    job = db.relationship('Job', backref=db.backref('characters'), order_by=id, info={'include': True})


@api_type
class Character(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    team_id = db.Column(db.Integer, db.ForeignKey('raid_team.id'))
    team = db.relationship('RaidTeam', backref=db.backref('members'), order_by=id)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('characters'), order_by=id)
    server_id = db.Column(db.Integer, db.ForeignKey('server.id'))
    server = db.relationship('Server', backref=db.backref('characters'), order_by=id)
    jobs = association_proxy('character_jobs', 'job')

    @staticmethod
    def startswith(prefix):
        if len(prefix) < 3:
            return []
        return Character.query.filter(Character.name.like(prefix + '%')).limit(10).all()

    def __str__(self):
        return self.name


@api_type
class RaidTeam(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    owner = db.relationship('User', backref=db.backref('owned_teams'), order_by=name)

    def __str__(self):
        return self.name


@api_type
class Raid(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    short_name = db.Column(db.String)
    name = db.Column(db.String)
    size = db.Column(db.Integer, default=8)

    def __str__(self):
        return self.name


class InviteStatus(RaidAppEnum):
    accepted = 1
    invited = 2
    maybe = 3
    rejected = 4


@api_type
class Server(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    region = db.Column(db.String)

    @staticmethod
    def startswith(prefix):
        return Server.query.filter(Server.name.like(prefix + '%')).limit(10).all()


@api_type
class TeamInvite(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    inviter_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    inviter = db.relationship('User', backref=db.backref('outgoing_invitations'))
    invitee_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    invitee = db.relationship('Character', backref=db.backref('incoming_invitations'))
    team_id = db.Column(db.Integer, db.ForeignKey('raid_team.id'))
    team = db.relationship('RaidTeam', backref=db.backref('invitations'), order_by=id)


@api_type
class Invite(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship('Event', backref=db.backref('invites'), order_by=id)
    character_id = db.Column(db.Integer, db.ForeignKey('character.id'))
    character = db.relationship('Character', backref=db.backref('invites'), order_by=id)
    _status = db.Column(InviteStatus.to_sqla_enum())

    @property
    def status(self):
        return InviteStatus.from_db(self._role)

    @status.setter
    def status(self, status):
        if isinstance(status, RaidAppEnum):
            self._status = status.format_for_db()
        else:
            self._status = status

    def __str__(self):
        return '{} invited to {} ({})'.format(
            self.player.name,
            self.event.name,
            self.status)


@api_type
class User(RaidAppModel, db.Model):
    serialise_fields = ['id', 'name']
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    password = db.Column(db.String)
    email = db.Column(db.String, unique=True)

    def register(self):
        if User.where(email=self.email).count() != 0:
            raise errors.UserExistsError('A user with this email address already exists')
        self.password = bcrypt.hashpw(self.password.encode('utf-8'), bcrypt.gensalt(16)).decode('ascii')
        User.save(self)

@api_type
class Session(User):
    __tablename__ = 'user'

    @staticmethod
    def login(email, password):
        user = Session.where(email=email).first()
        saved_password = user.password.encode('ascii')
        if user and bcrypt.hashpw(password.encode('utf-8'), saved_password) == saved_password:
            return user
        else:
            raise errors.InvalidLoginError('Invalid email or password', pointer='/data')


@api_type
class Event(RaidAppModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    raid_id = db.Column(db.Integer, db.ForeignKey('raid.id'))
    raid = db.relationship('Raid', backref=db.backref('events'), order_by=id)

    def __str__(self):
        return self.name
