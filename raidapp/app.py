from functools import wraps
from uuid import uuid4
import logging

from flask import Flask, json, jsonify, g, request
from flask.ext.sqlalchemy import SQLAlchemy

from .config import config
from .errors import APIError
from .utils import type_key, RecordWithInclusions



app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = config['db_uri']
app.secret_key = config.get('secret_key', 'not at all secret')
db = SQLAlchemy(app)


class RaidJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, 'to_json'):
            return o.to_json()
        return super().default(o)


class RaidJSONDecoder(json.JSONDecoder):
    types = {}

    def __init__(self, *args, **kwargs):
        super_kwargs = {'object_hook': self.obj_from_dict}
        super_kwargs.update(kwargs)
        super().__init__(*args, **super_kwargs)

    def obj_from_dict(self, dct):
        if 'data' in dct and not isinstance(dct['data'], dict):
            return dct['data']
        if 'type' in dct and dct['type'] in self.types:
            return self.types[dct['type']].from_json(dct)

        return dct

    @classmethod
    def register(cls, type, name=None):
        if not name:
            name = type_key(type)
        cls.types[name] = type
        return type


app.json_encoder = RaidJSONEncoder
app.json_decoder = RaidJSONDecoder


@app.errorhandler(APIError)
def handle_api_error(error):
    dct = error.to_dict()
    json = {
        'errors': [dct],
    }
    if g.request_id:
        json['meta'] = {
            'request_id': g.request_id
        }
    response = jsonify(json)
    response.status_code = error.status_code
    return response


@app.before_request
def setup_request():
    g.request_id = uuid4()
    logging_level = logging.DEBUG if app.debug else logging.INFO
    g.logger = logging.getLogger('app.request.{}'.format(str(g.request_id)))
    g.logger.setLevel(logging_level)
    ch = logging.StreamHandler()
    ch.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s [{}]'.format(g.request_id))
    ch.setFormatter(formatter)
    g.logger.addHandler(ch)


def api_type(cls, name=None):
    RaidJSONDecoder.register(cls, name=name)
    return cls


def get_response_for_route(f, func_args, func_kwargs):
    meta = {'request_id': g.request_id} if g.request_id else None
    json_result = {
        'meta': meta
    }
    result = f(*func_args, **func_kwargs)

    if hasattr(result, 'to_json_result'):
        json_result = result.to_json_result()
    else:
        json_result['data'] = result
    return jsonify(**json_result)


def alter_code_if_needed(request, response):
    if request.method == 'POST' and response.status_code == 200:
        response.status_code = 201


def api_route(*route_args, sensitive=False, app=app, **route_kwargs):
    def decorator(f):
        @app.route(*route_args, **route_kwargs)
        @wraps(f)
        def wrapped(*func_args, **func_kwargs):
            g.logger.info('{} {}'.format(request.method, request.path))
            if not sensitive and request.data:
                g.logger.debug(request.data)

            response = get_response_for_route(f, func_args, func_kwargs)
            alter_code_if_needed(request, response)
            g.logger.info('Result: {}'.format(response.get_data()))
            return response

        return wrapped

    return decorator


